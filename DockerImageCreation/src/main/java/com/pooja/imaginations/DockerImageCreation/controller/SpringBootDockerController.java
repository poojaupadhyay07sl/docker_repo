package com.pooja.imaginations.DockerImageCreation.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringBootDockerController {

    @GetMapping("/docker")
    public String getMessage(){
        return "Spring Boot Docker Example";
    }
}
